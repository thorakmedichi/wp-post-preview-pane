<?php

/*
Plugin Name: Preview Pane for Posts
Plugin URI:
Description: This plugin changes the way post and page listings display. It splits the main content area into post listings on the left and the selected posts preview on the right.
Version: 1.0
Author: Ryan Stephens
Author URI: http://www.sketchpad-media.com
*/

/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/*
	PLUGIN SETTINGS
*/

define ('PPP_PLUGIN_URL', plugins_url('/', __FILE__));
define ('PPP_PLUGIN_DIR', plugin_dir_path(__FILE__));

if ( is_admin() && (basename($_SERVER["SCRIPT_NAME"]) != 'upload.php') ){
						
	/*
		AJAX INTEGRATION: 
		http://tommcfarlin.com/introducing-ajax-in-the-wordpress-dashboard/
		http://codex.wordpress.org/AJAX_in_Plugins
	*/
	// embed my javascript file that makes the AJAX request
	wp_enqueue_script ('preview-pane-functions', plugins_url( '/javascript/functions.js', __FILE__ , array ( 'jquery' )));
	// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php) and pass our variables to use
	wp_localize_script('preview-pane-functions', 'ajax_preview_pane_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'post_id' => $_GET['post'], 'ajax_request' => 'true' ) );
	
	
	/* 
		SETUP STYLES for the plugin
	*/
	wp_enqueue_style  ('preview-pane-css', plugins_url( '/css/preview-pane-layout.css', __FILE__ ));
	
}

// add_action( 'wp_ajax_AJAX_OBJECT', 'PHP Function Name' )
add_action( 'wp_ajax_get_preview_pane_content', 'generate_preview_pane_content' ); // used if user is logged in
function generate_preview_pane_content (){
	global $wpdb;
	
	$post_preview_id = $_GET['post_id'];
	$post_preview_id = str_replace('post-', '', $post_preview_id);
	
    $permalink = get_permalink( $post_preview_id );
	
	/*
	* UNCOMMENT IF YOU ONLY WANT TO DISPLAY PART OF THE PAGE. IE MAIN CONTENT DIV
	*/
	/*
	// Get the remote content
	//$output = file_get_contents ($permalink);
	$curl = curl_init( $permalink );
	curl_setopt($curl, CURLOPT_ENCODING, '');
	curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	$output = iconv("UTF-8", "CP1252", curl_exec($curl)); // Fix the encoding issues. ' became the euro symbol etc.
	curl_close($curl);
	
	// Display only parts of the DOM
	$DOM = new DOMDocument;
	$DOM->recover = true;
	$DOM->strictErrorChecking = false;
	@$DOM->loadHTML( $output );
	
	//get the content of the page
	$content = $DOM->getElementById('content');
	
	//display the content
	$new_output = DOMinnerHTML ($content);
	*/
	
	/*
	* UNCOMMENT IF YOU WANT TO DISPLAY THE WHOLE PAGE IN ALL ITS GLORY
	*/
	$new_output = '<iframe src="'.$permalink.'" style="width:100%; height:100%;"></iframe>';
	
	echo $new_output;
	
	die();	
}

function DOMinnerHTML($element) { 
   $innerHTML = ""; 
   $children = $element->childNodes; 
   foreach ($children as $child) 
   { 
      $tmp_dom = new DOMDocument(); 
      $tmp_dom->appendChild($tmp_dom->importNode($child, true)); 
      $innerHTML.=trim($tmp_dom->saveHTML()); 
   } 
   return $innerHTML; 
}