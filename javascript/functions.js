jQuery(document).ready(function() {
	/*
	* PREVIEW PANE ADMIN POST LIST
	* Fun little tweak with the admin post listing page.
	*/
	
	
	// Setup the main display
	$('#posts-filter table').css('width','20%'); // Set the post listings to 30% width
	$('#posts-filter table').css('float','left'); // Float the post listings all the way left
	$('#posts-filter table thead th, #posts-filter table tfoot th').each( function () { // Hide all the column headings
		if ( ($(this).attr('id') != 'cb') && ($(this).attr('id') != 'title') ) {
			$(this).remove();	
		}
	});
	$('#posts-filter table tbody td').each( function () { // Hide all the column data
		if ( !$(this).hasClass('post-title') ) {
			$(this).hide();	
		}
	});
	$('#posts-filter .row-actions').each( function () { // Hide all the row actions like 'View' and 'Trash'
			$(this).hide();	
	});
	

	
	// create a new div element to hold the preview pane if it doesnt already exist
	if ($('#preview-pane').length === 0){
		//$('#posts-filter table').after('<div id="preview-pane"></div>');
		$('#posts-filter table').after('<div id="preview-pane" style="height:100%;"></div>');
	}

	// When the user rolls over the table row lets load the data from the page
	$('#the-list').on ('mouseenter', 'tr', function (){
		var post_id = $(this).attr('id');	// get the posts id
		var row_actions = $('.row-actions', this).clone();

		// Setup our vars to use with AJAX in Wordpress
		var data = {
			action: 'get_preview_pane_content',	// This is needed for the PHP function. If we dont pass this the results will always show a '0' as well
			post_id: post_id
		};
		
		jQuery.ajax({
				url: ajax_preview_pane_object.ajaxurl,
				type: 'get',
				data: data,
				dataType: 'html',
				success: function( return_data ){ 
					
					// Show the captured page
					$('#preview-pane').html (return_data); // Update the preview pane with the new HTML content
					
					// Move the row actions above the preview pane
					$(row_actions).prependTo("#preview-pane");
					
					$('#preview-pane .row-actions').css('visibility', 'visible');
					$('#preview-pane .row-actions').css('display', 'block');
					$('#preview-pane .row-actions').css('float', 'right');
					$('#preview-pane .row-actions').css('margin-right', '20px');
				}
		});
		 
		 
	});
	
	
	
});